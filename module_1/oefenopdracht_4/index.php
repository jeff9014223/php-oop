<?php 

class Product {
    public $name;
    public $category;

    public function setName($name) {
        $this->name = strtolower($name);
    }

    public function setCategory($name) {
        $this->category = strtoupper($name);
    }
}

$product = new Product();
$product->setName("Test");
$product->setCategory("Test categorie");

?>

Naam: <?= $product->name; ?>
<br>
Categorie: <?= $product->category ?>