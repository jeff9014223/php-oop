<?php 

class Product {
    public function __construct(
        public $name,
        public $description,
        public $category = "dranken",
        public $rating = 3
    ) {}
}

$product1 = new Product( name: "Product 1", category: "test", description: "product1 description", rating: 2 );  
$product2 = new Product( name: "Product 2", category: "test1", description: "product2 description", rating: 4 );
$product3 = new Product( name: "Product 3", category: "test2", description: "product3 description", rating: 5 );
?>

<h1>Product 1</h1>
<?= $product1->category; ?>
<br>
<?= $product1->description; ?>
<br>
<?= $product1->rating; ?>

<h1>Product 2</h1>
<?= $product2->category; ?>
<br>
<?= $product2->description; ?>
<br>
<?= $product2->rating; ?>

<h1>Product 3</h1>
<?= $product3->category; ?>
<br>
<?= $product3->description; ?>
<br>
<?= $product3->rating; ?>