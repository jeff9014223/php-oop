<?php 

class Product {
    public $price;

    public function showSalesPrice() {
        return $this->price / 100 * 9 + $this->price;
    }
}

$product = new Product();
$product->price = 100;

?>

Prijs: <?= $product->price ?>
<br>
Prijs inclusief BTW (9%): <?= round($product->showSalesPrice(), 2) ?>