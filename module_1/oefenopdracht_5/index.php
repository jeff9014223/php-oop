<?php 

class Product {
    public $category;
    public $description;

    public function __construct($category, $description) {
        $this->category = strtolower($category);
        $this->description = $description;
    }
}

$product = new Product("test", "test2");   
?>

<?= $product->category; ?>
<br>
<?= $product->description; ?>