<?php 

class Product {
    public $prijs;
    public $naam;
    public $voorraad;
}

$product = new Product();

$product->prijs = 5;
$product->naam = "Test";
$product->voorraad = 2;

?>

Prijs: <?= $product->prijs ?>
<br>
Naam: <?= $product->naam ?>
<br>
Voorraad: <?= $product->voorraad ?>

<?php

$product->prijs = 10;

?>

<br>
Nieuwe waarde: <?= $product->prijs ?>