<?php 

class BankAccount
{
    public function __construct(public $balance, public $accountNumber) {}

    public function setAccountNumber($accountNumber): string {
        $this->accountNumber = $accountNumber;
        return "Uw bankaccount nummer is gewijzigd in " . $this->accountNumber;
    }    

    public function getAccountNumber(): string {
        return "Uw bankaccount nummer is: " . $this->accountNumber;
    }

    public function setBalance($balance): void {
        $this->balance = $balance;
    }

    public function getBalance(): string {
        return "Uw huidige saldo is: " . $this->balance;
    }

    public function withdraw($amount): string {
        if ($amount > $this->balance) return "U heeft onvoldoende saldo om " . $amount . " euro op te nemen.";
        $this->balance -= $amount;
        return "U heeft " . $amount . " euro opgenomen. Uw huidige saldo is: " . $this->balance;
    }

    public function deposit($amount): string {
        $this->balance += $amount;
        return "U heeft " . $amount . " op uw bankaccount gezet. Uw huidige saldo is: " . $this->balance;
    }
}

$account = new BankAccount(1000, "NL01INHO0000000001");
echo $account->getAccountNumber();
echo "<br>";
echo $account->getBalance();
echo "<br>";
echo $account->withdraw(15);
echo "<br>";
echo $account->deposit(20);
echo "<br>";
echo $account->withdraw(1500);
echo "<br>";
echo $account->setAccountNumber("NL01INHO0000000002");