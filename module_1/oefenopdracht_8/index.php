<?php 

class Product {

    public $name;
    public $category;
    public $description;
    public $rating;

    public function __construct($name, $description, $category = "dranken", $rating = 3) {
        $this->category = strtolower($category);
        $this->description = $description;
        $this->rating = $rating;
        $this->name = $name;
    }

    public function getProduct() {
        return "
            <h1>$this->name</h1>
            $this->category
            <br>
            $this->description
            <br>
            $this->rating
        ";
    }
}

$product = new Product( name: "Product 1", category: "test", description: "product1 description", rating: 2 ); 

echo $product->getProduct();