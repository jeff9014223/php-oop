<?php 

class Product {

    public $name;
    public $category;
    public $description;
    public $rating;

    public function __construct($name, $description, $category = "dranken", $rating = 3) {
        $this->category = strtolower($category);
        $this->description = $description;
        $this->rating = $rating;
        $this->name = $name;
    }
}

$product1 = new Product( name: "Product 1", category: "test", description: "product1 description", rating: 2 );  
$product2 = new Product( name: "Product 2", category: "test1", description: "product2 description", rating: 4 );
$product3 = new Product( name: "Product 3", category: "test2", description: "product3 description", rating: 5 );
?>

<h1>Product 1</h1>
<?= $product1->category; ?>
<br>
<?= $product1->description; ?>
<br>
<?= $product1->rating; ?>

<h1>Product 2</h1>
<?= $product2->category; ?>
<br>
<?= $product2->description; ?>
<br>
<?= $product2->rating; ?>

<h1>Product 3</h1>
<?= $product3->category; ?>
<br>
<?= $product3->description; ?>
<br>
<?= $product3->rating; ?>