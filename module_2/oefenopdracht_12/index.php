<?php
declare(strict_types=1);

class Game
{
    public string $name;
    public string $description;
    public float $price;
    public array $tags;

    public function __construct(string $name, string $description, float $price, array $tags = []) {
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->tags = $tags;
    }

    public function getProperties()
    {
        return "Naam: " . $this->name . "<br>" . "Beschrijving: " . $this->description . "<br>" . "Prijs: " . $this->price . "<br>" . "Tags: " . implode(", ", $this->tags);
    }
}

?>