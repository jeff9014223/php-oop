<?php

class Game
{
    public function __construct(public $name, public $description, public $price, public $tags = []) {}

    public function getProperties()
    {
        return "Naam: " . $this->name . "<br>" . "Beschrijving: " . $this->description . "<br>" . "Prijs: " . $this->price . "<br>" . "Tags: " . implode(", ", $this->tags);
    }
}

$g1 = new Game( name: "object1", description: "test", price: "70", tags: ["test1", "test2"]  );
$g2 = new Game( name: "object2", description: "test", price: "70", tags: ["test1", "test2"]  );
$g3 = new Game( name: "object3", description: "test", price: "70", tags: ["test1", "test2"]  );

echo $g1->getProperties();
echo "<br>";
echo $g2->getProperties();
echo "<br>";
echo $g3->getProperties();

?>