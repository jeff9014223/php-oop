<?php

class Product
{
    protected string $name;
    protected string $description;
    protected string $brand;
    protected float $price;
    protected string $productType;

    public function __construct(string $name, string $description, string $brand, float $price)
    {
        $this->name = $name;
        $this->description = $description;
        $this->brand = $brand;
        $this->price = $price;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function printInfo(): string
    {
        $info = $this->getInfo();
        $infoString = "";
        foreach ($info as $key => $value) {
            if (is_array($value)) {
                $value = implode(", ", $value);
            }
            $infoString .= "$key: $value<br>";
        }
        return $infoString;
    }

    protected function getProductType(): string
    {
        return $this->productType;
    }
}

class Game extends Product
{
    private string $genre;
    private array $requirements = [];

    public function __construct(string $name, string $description, string $brand, float $price, string $genre)
    {
        parent::__construct($name, $description, $brand, $price);
        $this->genre = $genre;
        $this->productType = "Game";
    }

    public function addRequirement(string $requirement): string
    {
        $this->requirements[] = $requirement;
        return "Requirement toegevoegd";
    }

    public function removeRequirement(string $requirement): string
    {
        $key = array_search($requirement, $this->requirements);
        if ($key !== false) {
            unset($this->requirements[$key]);
            return "Requirement niet gevonden";
        }
        return "Requirement verwijderd";
    }

    public function getInfo(): array
    {
        $recommendedAge = $this->calculateRecommendedAge();
        return [
            "name" => $this->name,
            "description" => $this->description,
            "brand" => $this->brand,
            "price" => $this->price,
            "genre" => $this->genre,
            "requirements" => $this->requirements,
            "recommendedAge" => $recommendedAge
        ];
    }

    private function calculateRecommendedAge(): string
    {
        if ($this->genre === "Shooter") {
            return "18+";
        } else {
            return "3+";
        }
    }
}

class Music extends Product {
    private array $numbers = [];
    private int $durationInMinutes;

    public function __construct(string $name, string $description, string $brand, float $price, int $durationInMinutes)
    {
        parent::__construct($name, $description, $brand, $price);
        $this->durationInMinutes = $durationInMinutes;
        $this->productType = "Music";
    }

    public function addNumber(string $number): string
    {
        $this->numbers[] = $number;
        return "Nummer toegevoegd";
    }

    public function removeNumber(string $number): string
    {
        $key = array_search($number, $this->numbers);
        if ($key !== false) {
            unset($this->numbers[$key]);
            return "Nummer verwijderd";
        }
        return "Nummer niet gevonden";
    }

    public function getInfo(): array
    {
        $durationInHours = $this->calculateDurationInHours();
        return [
            "name" => $this->name,
            "description" => $this->description,
            "brand" => $this->brand,
            "price" => $this->price,
            "numbers" => $this->numbers,
            "durationInHours" => $durationInHours
        ];
    }

    private function calculateDurationInHours(): float
    {
        return $this->durationInMinutes / 60;
    }
}

class Movie extends Product
{
    private string $quality;

    public function __construct(string $name, string $description, string $brand, float $price, string $quality)
    {
        parent::__construct($name, $description, $brand, $price);
        $this->quality = $quality;
        $this->productType = "Movie";
    }

    public function getInfo(): array
    {
        $IMDbRating = $this->simulateIMDbRating();
        return [
            "name" => $this->name,
            "description" => $this->description,
            "brand" => $this->brand,
            "price" => $this->price,
            "quality" => $this->quality,
            "IMDbRating" => $IMDbRating
        ];
    }

    private function simulateIMDbRating(): float
    {
        return rand(1, 10);
    }
}

class Catalog
{
    private array $products = [];

    public function addProduct(Product $product): string
    {
        $this->products[] = $product;
        return "Product toegevoegd";
    }

    public function removeProduct(Product $product): string
    {
        $key = array_search($product, $this->products);
        if ($key !== false) {
            unset($this->products[$key]);
            return "Product verwijderd";
        }
        return "Product toegevoegd";
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function getAveragePrice(): float
    {
        $totalPrice = 0;
        foreach ($this->products as $product) {
            $totalPrice += $product->getPrice();
        }
        return $totalPrice / count($this->products);
    }
}

$catelog = new Catalog();
$film_1 = new Movie("Film1", "Film1 description", "Film1 merk", 10.99, "4K");
$film_2 = new Movie("Film2", "Film2 description", "Film2 merk", 9.99, "HD");
$game_1 = new Game("Game1", "Game1 description", "Game1 merk", 59.99, "Shooter");
$game_2 = new Game("Game2", "Game2 description", "Game2 merk", 49.99, "RPG");
$music_1 = new Music("Music1", "Music1 description", "Music1 merk", 9.99, 60);
$music_2 = new Music("Music2", "Music2 description", "Music2 merk", 9.99, 120);

$catelog->addProduct($film_1);
$catelog->addProduct($film_2);
$catelog->addProduct($game_1);
$catelog->addProduct($game_2);
$catelog->addProduct($music_1);
$catelog->addProduct($music_2);

// foreach ($catelog->getProducts() as $product) {
//     echo $product->printInfo();
//     echo "<br>";
// }

$catelog->removeProduct($film_1);

foreach ($catelog->getProducts() as $product) {
    echo $product->printInfo();
    echo "<br>";
}