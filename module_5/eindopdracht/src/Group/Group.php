<?php

namespace Group;

class Group
{
    protected string $naam;

    public function __construct(string $naam)
    {
        $this->naam = $naam;
    }

    public function getName(): string
    {
        return $this->naam;
    }
}