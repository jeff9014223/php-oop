<?php

namespace Person;

use Student\Student;

abstract class Person
{
    protected string $naam;
    protected int $leeftijd;

    public static float $totalCollectedAmount = 0;

    public function __construct(string $naam, int $leeftijd)
    {
        $this->naam = $naam;
        $this->leeftijd = $leeftijd;
    }

    public static function collectPayment(float $amount, Person $instance)
    {
        if ($instance instanceof Student) {
            self::$totalCollectedAmount += $amount;
            $instance->heeftBetaald = true;
        }
    }

    public function getName(): string
    {
        return $this->naam;
    }
}