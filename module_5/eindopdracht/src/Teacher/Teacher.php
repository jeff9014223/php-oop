<?php

namespace Teacher;

use Person\Person;

class Teacher extends Person
{
    public static $docenten = [];

    public function __construct(string $naam, int $leeftijd)
    {
        parent::__construct($naam, $leeftijd);
        self::$docenten[] = $this;
    }
}