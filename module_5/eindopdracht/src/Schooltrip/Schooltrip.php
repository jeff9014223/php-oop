<?php

namespace Schooltrip;

use Student\Student;
use Teacher\Teacher;
use Schooltrip\SchooltripList;

class Schooltrip
{
    protected string $plaats;
    protected array $excursieLijsten = [];

    public function __construct(string $plaats)
    {
        $this->plaats = $plaats;
    }

    public function addSchooltripList()
    {
        $excursieLijst = new SchooltripList();
        $docent = array_shift(Teacher::docenten);
        $excursieLijst->addPerson($docent);
        $this->excursieLijsten[] = $excursieLijst;
    }

    public function addStudent(Student $student)
    {
        $laatsteExcursieLijst = end($this->excursieLijsten);
    
        if ($laatsteExcursieLijst instanceof SchooltripList) {
            $laatsteExcursieLijst->addPerson($student);
        }
    }
}