<?php

namespace Schooltrip;

use Student\Student;
use Teacher\Teacher;
use Person\Person;

class SchooltripList
{
    protected array $docenten = [];
    protected array $studenten = [];

    public function addPerson($person)
    {
        if ($person instanceof Teacher) {
            $this->docenten[] = $person;
        } elseif ($person instanceof Student) {
            Person::collectPayment(20, $person);
            $this->studenten[] = $person;
        }
    }

    public function getTeachers()
    {
        return $this->docenten;
    }

    public function getStudents()
    {
        return $this->studenten;
    }

    public function generateTripList()
    {
        $excursieInfo = [
            "studenten" => [],
            "docenten" => [],
            "totaalOpgehaald" => 0,
        ];

        $studenten = $this->getStudents();
        foreach ($studenten as $student) {
            $excursieInfo["studenten"][] = [
                "naam" => $student->getName(),
                "klas" => $student->getGroup()->getName(),
                "heeftBetaald" => $student->heeftBetaald
            ];
            $excursieInfo["totaalOpgehaald"] += 20;

            if (count($excursieInfo["studenten"]) % 5 == 0) {
                $docent = array_shift($this->docenten);
                $excursieInfo["docenten"][] = [
                    "naam" => $docent->getName()
                ];
            }
        }

        return $excursieInfo;
    }
}
