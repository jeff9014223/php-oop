<?php

namespace Student;

use Person\Person;
use Group\Group;

class Student extends Person
{
    protected Group $groep;
    public bool $heeftBetaald = false;

    public function __construct(string $naam, int $leeftijd, Group $groep)
    {
        parent::__construct($naam, $leeftijd);
        $this->groep = $groep;
    }

    public function getGroup(): Group
    {
        return $this->groep;
    }
}