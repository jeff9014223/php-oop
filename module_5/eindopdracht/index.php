<?php

require_once __DIR__ . "/vendor/autoload.php";

use Student\Student;
use Teacher\Teacher;
use Group\Group;
use Schooltrip\SchooltripList;

$groep = new Group("groep 1");
$student1 = new Student("Student 1", 17, $groep);
$student2 = new Student("Student 2", 18, $groep);
$student3 = new Student("Student 3", 19, $groep);
$student4 = new Student("Student 4", 20, $groep);
$student5 = new Student("Student 5", 21, $groep);

$docent1 = new Teacher("Docent 1", 32);
$docent2 = new Teacher("Docent 2", 33);

$excursieLijst = new SchooltripList();
$excursieLijst->addPerson($student1);
$excursieLijst->addPerson($student2);
$excursieLijst->addPerson($student3);
$excursieLijst->addPerson($student4);
$excursieLijst->addPerson($student5);
$excursieLijst->addPerson($docent1);
$excursieLijst->addPerson($docent2);

?>

<html>
    <head>
        <title>Eindopdracht 5</title>
        <script src="https://cdn.tailwindcss.com"></script>
    </head>
    <body class="bg-zinc-900 w-screen h-screen text-white">
        <?php $tripList = $excursieLijst->generateTripList() ?>
        <div class="p-8">
            <table class="table-auto">
                <thead>
                    <tr>
                        <th class="px-4 py-2">Naam</th>
                        <th class="px-4 py-2">Klas</th>
                        <th class="px-4 py-2">Heeft betaald</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tripList["studenten"] as $student): ?>
                        <tr>
                            <td class="border px-4 py-2"><?= $student["naam"] ?></td>
                            <td class="border px-4 py-2"><?= $student["klas"] ?></td>
                            <td class="border px-4 py-2"><?= $student["heeftBetaald"] ? "Ja" : "Nee" ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="p-8">
            <table class="table-auto">
                <thead>
                    <tr>
                        <th class="px-4 py-2">Naam</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tripList["docenten"] as $docent): ?>
                        <tr>
                            <td class="border px-4 py-2"><?= $docent["naam"] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="p-8 text-lg">
            Totaal opgehaald: <?= $tripList["totaalOpgehaald"] ?>
        </div>
    </body>
</html>